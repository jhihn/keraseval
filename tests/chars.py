
import sys
sys.path.append("..")
sys.path.append("/opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages")
print sys.path

#from PIL import Image
from skimage.io import *
from skimage.color import *
from network_out import network_json

#image = Image.read("0123456789.png").convert('F')
image = rgb2gray(imread("0123456789.png"))

y_start=6
char_width=12
char_height=20

for i in range(10):
	char = []
	for y in range(char_height):
		for x in range(char_width):
			char.append(image[y+y_start][int(i * 11.5)+x])
				
	scores = network_json(char)
	winner = scores.index(max(scores))
	print "char: {0}, predicted: {1}\n".format(i, winner)

