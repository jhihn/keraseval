#!/usr/bin/env python
import sys, os, json, abc

class Language:
	Unknown = 0
	C = 1,
	CppQt = 2,
	Cpp = 3,
	Python = 4,
	JavaScript = 5
	_d = {
		'unknown': Unknown,
		'c': C,
		'c++qt': CppQt,
		'c++': Cpp,
		'python': Python,
		'javascript': JavaScript
	}
	@staticmethod
	def byName(name):
		return Language._d[name.lower()]

class LanguageWriter(object):
	__metaclass__ = abc.ABCMeta
	
	@abc.abstractmethod
	def writeFileHeader(*file): pass
	
	@abc.abstractmethod
	def writeActivationFunction(file, activationFunc): pass
	
	@abc.abstractmethod
	def writeFunctionOpening(file, functionName): pass
	
	@abc.abstractmethod
	def writeLayer(file, layer, object, weights, biases): pass
	
	@abc.abstractmethod
	def writeFunctionClosing(file): pass


class QtCppLanguageWriter(LanguageWriter):
	def writeFileHeader(self, file):
		file.write("#include <QVector>\n")
		file.write("#include <math.h>\n")
		file.write("\n")
		
	def writeActivationFunction(self, file, activationFunc):
		# use this for custom functions
		return
	
	def writeFunctionOpening(self, file, functionName):
		file.write("QVector<float> {0}(const QVector<float> &inputs)".format(functionName))
		file.write("{\n")
	
	def writeLayer(self, file, layer, config, weights, biases):
		outSize = config["units"]
		file.write("\tQVector<float> l{0} = {1}\n".format(layer, '{'))

		if layer == 0:
			inputVar = 'inputs'
		else:
			inputVar = 'l'+str(layer)
			
		for o in range(outSize):
			#file.write("\t\t{0}(".format(config["activation"]))
			for i in range(len(weights)):
				file.write("({2}[{0}] * {1}) + ".format(i, weights[i][o], inputVar))
			
			file.write("{0}".format(biases[o]))
			file.write( "\n" if o == outSize-1 else ",\n" )
		file.write("\t};\n")
		
		if config["activation"] == "softmax": #compute sum only once if softmax
			file.write("\tfloat sum{0} = 0; for (auto v: vals) sum{0} += v;\n".format(layer));

		file.write("\tQVector<float> la{0} = {1}\n".format(layer, '{'))
		for o in range(outSize):
			file.write("\t\t")
			# do the simple activation functions in-place
			if (config["activation"] == "identity"):
				file.write("l{0}[{1}]".format(layer, o))
			elif (config["activation"] ==  "sigmoid"):
				file.write("1/(1+exp(-l{0}[{1}]))".format(layer, o))
			elif config["activation"] == 'softmax':
				file.write("l{0}[{1}] / sum{0}".format(layer, o))
			elif config["activation"] == "relu":
				file.write("qMax(0, l{0}[{1}])".format(layer, o))
			else:
				file.write("\t\t{0}(l{1}[{2}], l{1})".format(config["activation"], layer, o))
			file.write( "\n" if o == outSize-1 else ",\n" )
		file.write("\t};\n")

	
	def writeFunctionClosing(self, file, lastLayer):
		file.write("\treturn la{0};\n".format(lastLayer))
		file.write("}\n\n")
	

class CppLanguageWriter(LanguageWriter):
	def writeFileHeader(self, file):
		file.write("#include <vector>\n")
		file.write("#include <math.h>\n")
		file.write("\n")
	
	def writeActivationFunction(self, file, activationFunc):
		return
	
	def writeFunctionOpening(self, file, functionName):
		file.write("std::vector<float> {0}(const std::vector<float> &inputs)".format(functionName))
		file.write("{\n")
	
	def writeLayer(self, file, layer, config, weights, biases):
		outSize = config["units"]
		file.write("\tstd::vector<double> l{1} = {0}\n".format('{', layer))

		if layer == 0:
			inputVar = 'inputs'
		else:
			inputVar = 'l'+str(layer-1)
			
		for o in range(outSize):
			file.write("\t\t")
			
			for i in range(len(weights)):
				file.write("({2}[{0}] * {1}) + ".format(i, weights[i][o], inputVar))
			
			file.write("{0}".format(biases[o]))
			file.write( "\n" if o == outSize-1 else ",\n" )
		file.write("\t};\n")
		
		if config["activation"] == "softmax": #compute sum only once if softmax
			file.write("\tfloat sum{0} = 0; for (auto v: vals) sum{0} += v;\n".format(layer));

		file.write("\tstd::vector<float> la{0} = {1}\n".format(layer, '{'))
		for o in range(outSize):
			file.write("\t\t static_cast<float>(")
			# do the simple activation functions in-place
			if (config["activation"] == "identity"):
				file.write("l{0}[{1}]".format(layer, o))
			elif (config["activation"] ==  "sigmoid"):
				file.write("1/(1+exp(-l{0}[{1}]))".format(layer, o))
			elif config["activation"] == 'softmax':
				file.write("l{0}[{1}] / sum{0}".format(layer, o))
			elif config["activation"] == "relu":
				file.write("max(0, l{0}[{1}])".format(layer, o))
			else:
				file.write("\t\t{0}(l{1}[{2}], l{1})".format(config["activation"], layer, o))
			file.write( ")\n" if o == outSize-1 else "),\n" )
		file.write("\t};\n")
	
	def writeFunctionClosing(self, file, lastLayer):
		file.write("\treturn la{0};\n".format(lastLayer))
		file.write("}\n\n")

class JavaScriptLanguageWriter(LanguageWriter):
	def writeFileHeader(self, file):
		pass
	
	def writeActivationFunction(self, file, activationFunc):
		return
	
	def writeFunctionOpening(self, file, functionName):
		file.write("function {0}(inputs)".format(functionName))
		file.write("{\n")
	
	def writeLayer(self, file, layer, config, weights, biases):
		outSize = config["units"]
		file.write("\tvar l{0} = [\n".format(layer))

		if layer == 0:
			inputVar = 'inputs'
		else:
			inputVar = 'l'+str(layer-1)
			
		for o in range(outSize):
			file.write("\t\t")
			
			for i in range(len(weights)):
				file.write("({2}[{0}] * {1}) + ".format(i, weights[i][o], inputVar))
			
			file.write("{0}".format(biases[o]))
			file.write( "\n" if o == outSize-1 else ",\n" )
		file.write("\t];\n")
		
		if config["activation"] == "softmax": #compute sum only once if softmax
			file.write("\tsum{0} = 0; for (var v of l{0}) sum{0} += v;\n".format(layer));

		file.write("\tvar la{0} = [\n".format(layer))
		for o in range(outSize):
			file.write("\t\t")
			# do the simple activation functions in-place
			if (config["activation"] == "identity"):
				file.write("l{0}[{1}]".format(layer, o))
			elif (config["activation"] ==  "sigmoid"):
				file.write("1/(1+Math.exp(-l{0}[{1}]))".format(layer, o))
			elif config["activation"] == 'softmax':
				file.write("l{0}[{1}] / sum{0}".format(layer, o))
			elif config["activation"] == "relu":
				file.write("Math.max(0, l{0}[{1}])".format(layer, o))
			else:
				file.write("\t\t{0}(l{1}[{2}], l{1})".format(config["activation"], layer, o))
			file.write( "\n" if o == outSize-1 else ",\n" )
		file.write("\t];\n")
	
	def writeFunctionClosing(self, file, lastLayer):
		file.write("\treturn la{0};\n".format(lastLayer))
		file.write("}\n\n")	

class PythonLanguageWriter(LanguageWriter):
	def writeFileHeader(self, file):
		file.write("import math\n\n")
	
	def writeActivationFunction(self, file, activationFunc):
		return
	
	def writeFunctionOpening(self, file, functionName):
		file.write("def {0}(inputs)".format(functionName))
		file.write(":\n")
	
	def writeLayer(self, file, layer, config, weights, biases):
		outSize = config["units"]
		file.write("\tl{0} = [\n".format(layer))

		if layer == 0:
			inputVar = 'inputs'
		else:
			inputVar = 'l'+str(layer-1)
			
		for o in range(outSize):
			file.write("\t\t")
			
			for i in range(len(weights)):
				file.write("({2}[{0}] * {1}) + ".format(i, weights[i][o], inputVar))
			
			file.write("{0}".format(biases[o]))
			file.write( "\n" if o == outSize-1 else ",\n" )
		file.write("\t]\n")
		
		if config["activation"] == "softmax": #compute sum only once if softmax
			file.write("\tsum{0} = sum(l{0})\n".format(layer));

		file.write("\tla{0} = [\n".format(layer))
		for o in range(outSize):
			file.write("\t\t")
			# do the simple activation functions in-place
			if (config["activation"] == "identity"):
				file.write("l{0}[{1}]".format(layer, o))
			elif (config["activation"] ==  "sigmoid"):
				file.write("1/(1+math.exp(-l{0}[{1}]))".format(layer, o))
			elif config["activation"] == 'softmax':
				file.write("l{0}[{1}] / sum{0}".format(layer, o))
			elif config["activation"] == "relu":
				file.write("max(0, l{0}[{1}])".format(layer, o))
			else:
				file.write("\t\t{0}(l{1}[{2}], l{1})".format(config["activation"], layer, o))
			file.write( "\n" if o == outSize-1 else ",\n" )
		file.write("\t]\n")
	
	def writeFunctionClosing(self, file, lastLayer):
		file.write("\treturn la{0}\n".format(lastLayer))
		file.write("\n\n")
		
class NetworkWriter:
	file = None
	langWriter = None

	def __init__(self, file, language):
		self.file = file
		if language == Language.CppQt:
			self.langWriter = QtCppLanguageWriter()
		elif language == Language.Cpp:
			self.langWriter = CppLanguageWriter()
		elif language == Language.JavaScript:
			self.langWriter = JavaScriptLanguageWriter()
		elif language == Language.Python:
			self.langWriter = PythonLanguageWriter()

	def writeNetwork(self, kerasModel, functionName):
		activationFunctions = [] 
		configs = kerasModel["config"]
		for c in range(len(configs)):
			activationFunctionName = configs[c]["config"]["activation"]
			if activationFunctionName not in activationFunctions:
				activationFunctions.append(activationFunctionName)
		
		self.langWriter.writeFileHeader(self.file)
		
		for activationFunctionName in activationFunctions:
			self.langWriter.writeActivationFunction(self.file, activationFunctionName)
		
		self.langWriter.writeFunctionOpening(self.file, functionName)
		
		weights = kerasModel["weights"]
		for layer in range(0, len(weights), 2):
			self.langWriter.writeLayer(self.file, layer,
					kerasModel["config"][layer]["config"],
					weights[layer], weights[layer+1])
		
		self.langWriter.writeFunctionClosing(self.file, (len(weights) /2) -1)
	


#./keraseval.py network.json c++ | gcc -x c++ -std=c++11 -c -

def test_QtCpp():
	import scikit.image
	
	def maxIndex(values):
		return max(values, key=lambda k: values[k])
	
	# QVector<float> character
	# QImage image("IMEI_012345678901234")
	# 
	# int offsets[] = {45, 54, 66, 77, 89, 101, 112, 124, 135, 147, 159, 177, 189, 201, 213
	# character.fill(0, 240)
	# for (int c=0 c< 15 c++):
	# 	for (int y=0 y< 20 y++):
	# 		for (int x=0 x< 12 x++)
	# 			character[(y*12)+x] = qGray(image.pixel(offsets[c]+x, y+5)) / 255
	# 	
	# 
	# 	quint64 start = QDateTime::currentMSecsSinceEpoch()
	# 	QVector<float> out = network(character)
	# 	int time = QDateTime::currentMSecsSinceEpoch() - start
	# 	qDebug() << c << maxIndex(out) << out
	
def complete_keras_model(model):
	json = model.to_json()
	json["weights"] = model.get_weights()
	
	return json

def complete_model_to_code(model, outfilename, functionName, lanuage=Language.Cpp):
	writer = NetworkWriter(outFile, language)
	writer.writeNetwork(model, functionName)
	
if __name__  ==  '__main__':
	filename = sys.argv[1]
	object = json.load(open(filename))

	if len(sys.argv) > 2:
		language = Language.byName(sys.argv[2])
	else:
		language = Language.CppQt
	
	if len(sys.argv) > 3 and  sys.argv[3] != '-':
		outFile = open(sys.argv[3], 'w')
	else:
		outFile = sys.stdout
	
	#writer = NetworkWriter(outFile, language)
	#writer.writeNetwork(object, os.path.basename(filename).replace('.', '_'))
	complete_model_to_code(object, outFile, os.path.basename(filename).replace('.', '_'), language)


